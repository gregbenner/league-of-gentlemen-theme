<?php
/*
Template Name: Our Fleet
*/
?>

<?php get_header(); ?>

<div id="content">
			
				<div id="inner-content" class="wrap clearfix">
			
				<div id="main" class=" first clearfix" role="main">

<?php
	$args = array(
	'post_type' => 'fleet',
	'post_status' => 'publish',
	'posts_per_page' => -1
);
$posts = new WP_Query( $args ); ?>

<?php if ($posts -> have_posts()) : while ($posts -> have_posts()) : $posts -> the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">
	<div>
		<?php
			$images =& get_children( 'post_type=attachment&post_mime_type=image&post_parent=' . $post->ID );
			foreach( $images as $imageID => $imagePost )
			echo wp_get_attachment_image($imageID, 'thumbnail', false);
		?>
	</div>

	<section>
		<span class="backToTop"><a href="#top"></a></span>


		<?php $my_post_meta = get_post_meta($post->ID, 'car_make', true); 
		switch ($my_post_meta) {
		 case "Bentley": ?>
		  	 <img src='<?php echo get_template_directory_uri(); ?>/library/images/logos/bentley.svg' alt="Bentley | Logo" height="24" />
		 <?php 
		 break;
		 case "Daimler": ?>
		  	<img src='<?php echo get_template_directory_uri(); ?>/library/images/logos/daimler.svg' alt="Daimler | Logo" height="24" />
		 <?php
		 break;
		 case "Jaguar" : ?>
		 	 <img src='<?php echo get_template_directory_uri(); ?>/library/images/logos/jaguar.svg' alt="Jaguar | Logo" height="24" />
		 <?php
		 break;
		 case "MG" : ?>
		 	 <img src='<?php echo get_template_directory_uri(); ?>/library/images/logos/mg.svg' alt="MG | Logo" height="24" />
		 <?php
		 break;
		 case "Rolls Royce" : ?>
		 	 <img src='<?php echo get_template_directory_uri(); ?>/library/images/logos/RR.svg' alt="Rolls Royce | Logo" height="24" />
		 <?php
		 break;

		 default:
		 #default
		 } ?>

		<strong>Engine Size:</strong> <?php print_custom_field('engine_size'); ?><br />
		<strong>Cylinders:</strong> <?php print_custom_field('cylinders'); ?><br />
		<strong>Power Output:</strong> <?php print_custom_field('power_output'); ?><br />
		<?php $my_post_meta = get_post_meta($post->ID, 'rear_seats', true); if (!empty($my_post_meta)) { ?>		
		<strong>Rear Seats: </strong> <?php print_custom_field('rear_seats'); } ?>
		

		<?php the_content(); ?>
	</section> <!-- end article section -->


</article> <!-- end article -->

<?php endwhile; else : endif; ?>

<?php wp_reset_postdata(); ?>

</div> <!-- end #inner-content -->
    
</div> <!-- end #content -->

<?php get_footer(); ?>

		